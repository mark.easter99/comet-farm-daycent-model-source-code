This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
https://creativecommons.org/licenses/by-nc-sa/4.0/
Copyright 2021 by Colorado State University. 
All rights reserved.

# COMET-Farm DayCent Model Source Code 
The files included herein are for the version of the DayCent model used in the COMET-Farm tool. They were constructed and compiled under Fedora 28, kernel version 4.18.17-200.fc28.x86_64. The version is rev85.

Before compiling, you will need to run the following commands: 

chmod +x Makefile
chmod +x structC2com.pl
chmod +x structC2mod.pl
